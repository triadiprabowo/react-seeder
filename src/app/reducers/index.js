// import core module
import { combineReducers } from "redux"

// import reducers
import StatementReducer from './statement.reducer';

export default combineReducers({
	StatementReducer
});