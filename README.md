# React Seeder #

React seeder with PostCSS, Stylus, Babel ES2016, Stage-1 Preset, and polyfills.

### Dependencies ###
* webpack > v3
* jest > v22.4
* enzyme > v.3.3
* stylus > v0.5
* babel > v6
* cssnano v3
* react > v16
* react-dom > v16
* react-redux > v5
* redux > v3.7
* react-router > v4.2

#### Current Version ####
` 1.0.0 `

### How to set up? ###
` npm install`

### NPM Scripts ###

* `npm start` - start development server on port 4000
* `npm run postbuild:prod` - image compress postbuild with gulp (coming soon)
* `npm run build:dev` - development build
* `npm run build:prod` - compress + minify enabled
* `npm test` - testing component